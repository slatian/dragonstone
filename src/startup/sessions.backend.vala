public class Dragonstone.Startup.Sessions.Backend {
	public static void register_core_sessions(Dragonstone.SuperRegistry super_registry){
		print("[startup][sessions] Adding core sessions... \n");
		var session_registry = super_registry.session_registry;
		var main_store = super_registry.main_store;
		session_registry.register_session("core.default",new Dragonstone.Session.Default(main_store));
		session_registry.register_session("core.uncached",new Dragonstone.Session.Uncached(main_store));
		session_registry.register_session("test.tls_0",new Dragonstone.Session.Tls(main_store));
	}
}
