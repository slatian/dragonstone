public class Dragonstone.Startup.Finger.Backend {
	
	public static void setup_store(Dragonstone.SuperRegistry super_registry){
		var store_registry = super_registry.stores;
		print("[startup][finger][backend] setup_store()\n");
		print("[startup][finger][backend] adding finger store\n");
		var store = new Dragonstone.Store.Finger();
		store_registry.add_resource_store("finger://",store);
	}
	
	public static void setup_uri_autocompletion(Dragonstone.SuperRegistry super_registry){
		var uri_autoprefixer = super_registry.uri_autoprefixer;
		uri_autoprefixer.add("finger:","finger://");
		uri_autoprefixer.add("finger://","finger://");
	}
	
}
