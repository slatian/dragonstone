public class Dragonstone.Startup.Cache.Backend {

	public static void setup_about_page(Dragonstone.SuperRegistry super_registry){
		print("[startup][cache] setup_about_page\n");
		var about = super_registry.about_store;
		about.set_sub_store("cache",new Dragonstone.Store.AboutStore.FixedStatus("interactive/cache"));
	}
	
}
