public class Dragonstone.Startup.File.Gtk {
	public static void setup_views(Dragonstone.SuperRegistry super_registry){
		var view_registry = super_registry.gtk_view_registry;
		var translation = super_registry.translation;
		view_registry.add_view("dragonstone.directory",() => { return new Dragonstone.GtkUi.View.Directory(translation); });
		view_registry.add_rule(new Dragonstone.GtkUi.LegacyViewRegistryRule.resource_view("text/dragonstone-directory","dragonstone.directory"));
	}
}
