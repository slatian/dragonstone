
public class Dragonstone.GtkUi.Application : Gtk.Application {
	
	public Dragonstone.SuperRegistry? super_registry { get; protected set; default=null; }
	
	public Application() {
		Object (
			application_id: "com.gitlab.baschdel.Dragonstone",
			flags: ApplicationFlags.CAN_OVERRIDE_APP_ID | ApplicationFlags.HANDLES_COMMAND_LINE
		);
	}
	
	public void initalize() {
		if (super_registry != null) { return; }
		this.shutdown.connect(on_shutdown);
		super_registry = new Dragonstone.SuperRegistry();
		//TODO: remove this asm thingy together with the super_registry, was a bad idea
		//      integrate with settings (json) instead
		//Initalize core registries
		//initalize settings
		var default_settings_provider = new Dragonstone.Settings.RamProvider();
		var persistant_settings_provider = Dragonstone.Startup.Settings.Backend.get_file_settings_provider("","settings.", "persistant_settings_provider");
		var theme_settings_provider = Dragonstone.Startup.Settings.Backend.get_file_settings_provider("themes/","themes.","theme_settings_provider");
		var core_settings_provider = super_registry.core_settings_provider;
		core_settings_provider.add_fallback(persistant_settings_provider);
		core_settings_provider.add_fallback(theme_settings_provider);
		core_settings_provider.add_fallback(default_settings_provider);
		//setup setting logging
		core_settings_provider.submit_client_report.connect((report) => {
			print(@"[settings][client]$report\n");
		});
		core_settings_provider.provider_report.connect((report) => {
			print(@"[settings][provider]$report\n");
		});
		//Set defaults
		Dragonstone.Startup.Hypertext.Settings.register_default_settings(default_settings_provider);
		Dragonstone.Startup.Frontend.Settings.register_default_settings(default_settings_provider);
		Dragonstone.Startup.Bookmarks.Settings.register_default_settings(default_settings_provider);
		// Reinitialize bookmarks settings bridge
		super_registry.bookmarks_settings_bride = new Dragonstone.Settings.Bridge.Bookmarks(super_registry.core_settings_provider, "settings.bookmarks", super_registry.bookmarks);
		super_registry.frontend_settings = new Dragonstone.Settings.Bridge.KV(super_registry.core_settings_provider, "settings.frontend.kv");
		
		//register gophertypes
		Dragonstone.Startup.GopherWrite.Backend.setup_gophertypes(super_registry);
		//Initalize backends
		Dragonstone.Startup.Bookmarks.Backend.setup_about_page(super_registry);
		Dragonstone.Startup.Cache.Backend.setup_about_page(super_registry);
		Dragonstone.Startup.Gopher.Backend.setup_mimetypes(super_registry);
		Dragonstone.Startup.Gopher.Backend.setup_store(super_registry);
		Dragonstone.Startup.Gopher.Backend.setup_uri_autocompletion(super_registry);
		Dragonstone.Startup.GopherWrite.Backend.setup_store(super_registry);
		Dragonstone.Startup.Gemini.Backend.setup_mimetypes(super_registry);
		Dragonstone.Startup.Gemini.Backend.setup_store(super_registry);
		Dragonstone.Startup.Gemini.Backend.setup_uri_autocompletion(super_registry);
		Dragonstone.Startup.GeminiUpload.Backend.setup_store(super_registry);
		Dragonstone.Startup.GeminiWrite.Backend.setup_store(super_registry);
		Dragonstone.Startup.File.Backend.setup_store(super_registry);
		Dragonstone.Startup.File.Backend.setup_uri_autocompletion(super_registry);
		Dragonstone.Startup.Finger.Backend.setup_store(super_registry);
		Dragonstone.Startup.Finger.Backend.setup_uri_autocompletion(super_registry);
		Dragonstone.Startup.Utiltest.Backend.setup_about_page(super_registry);
		super_registry.stores.add_resource_store("test://", new Dragonstone.Store.Test());
		//Initalize sessions
		Dragonstone.Startup.Sessions.Backend.register_core_sessions(super_registry);
		//Initalize localization
		Dragonstone.Startup.Localization.English.setup_language(super_registry);
		Dragonstone.Startup.Localization.English.use_language(super_registry);
		//reinitialize view registry
		super_registry.gtk_view_registry = new Dragonstone.GtkUi.LegacyViewRegistry.default_configuration(super_registry.translation);
		//Initalize frontends
		Dragonstone.Startup.Bookmarks.Gtk.setup_views(super_registry);
		Dragonstone.Startup.Cache.Gtk.setup_views(super_registry);
		Dragonstone.Startup.Sessions.Gtk.setup_views(super_registry);
		Dragonstone.Startup.File.Gtk.setup_views(super_registry);
		Dragonstone.Startup.Hypertext.Gtk.setup_views(super_registry, core_settings_provider);
		Dragonstone.Startup.Gemini.Gtk.setup_views(super_registry);
		Dragonstone.Startup.Upload.Gtk.setup_views(super_registry);
		Dragonstone.Startup.Utiltest.Gtk.setup_views(super_registry);
		
	}
	
	protected override void activate() {
		initalize();
		build_window();
	}
	
	protected override int command_line(ApplicationCommandLine command_line) {
		initalize();
		Dragonstone.Window? window = (Dragonstone.Window) get_active_window();
		bool new_window = false;
		if (window == null) {
			window = build_window();
			new_window = true;
		}
		string session_id = "core.default";
		bool next_is_sessionid = false;
		bool firstarg = true;
		bool uri_opened = false;
		foreach (string arg in command_line.get_arguments()){
			if (firstarg) {
				firstarg = false;
			} else if (next_is_sessionid) {
				session_id = arg;
			} else if (arg == "--new-window") {
				if (!new_window) {
					if (!uri_opened) {
						window.add_new_tab();
					}
					uri_opened = false;
					window = build_window();
				}
				new_window = false;
			} else if (arg == "--session") {
				next_is_sessionid = true;
			} else {
				string uri = arg;
				var pwd = command_line.get_cwd();
				if (pwd != null) {
					if (!pwd.has_suffix("/")) {
						pwd = pwd+"/";
					}
					uri = Dragonstone.Util.Uri.join("file://"+pwd, arg);
				}
				window.add_tab(uri,session_id);
				uri_opened = true;
				new_window = false;
			}
		}
		if (!uri_opened) {
			window.add_new_tab();
		}
		return 0;
	}
	
	protected void on_shutdown() {
		super_registry.cache.erase();
		super_registry.session_registry.erase_all_caches();
		super_registry.bookmarks_settings_bride.export();
	}
	
	private Dragonstone.Window build_window() {
		var window = new Dragonstone.Window(this);
		add_window(window);
		return window;
	}
}
