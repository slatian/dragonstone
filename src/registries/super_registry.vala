public class Dragonstone.SuperRegistry : Object {
	
	public Dragonstone.Store.About about_store;
	public Dragonstone.Registry.BookmarkRegistry bookmarks;
	public Dragonstone.Settings.Bridge.Bookmarks bookmarks_settings_bride; //To be replaced
	public Dragonstone.Store.Cache cache; // To be removed, this shouldn't be global
	public Dragonstone.Settings.Context.Fallback core_settings_provider;
	public Dragonstone.Settings.Bridge.KV frontend_settings;
	public Dragonstone.Registry.GopherTypeRegistry gopher_type_registry;
	public Dragonstone.GtkUi.LegacyViewRegistry gtk_view_registry; //to be moved to somewhere else
	public Dragonstone.Interface.ResourceStore main_store;
	public Dragonstone.Registry.MimetypeGuesser mimeguesser;
	public Dragonstone.Registry.SessionRegistry session_registry;
	public Dragonstone.Registry.StoreRegistry stores;
	public Dragonstone.Registry.TranslationMultiplexerRegistry translation;
	public Dragonstone.Registry.UriAutoprefix uri_autoprefixer;
	
	public SuperRegistry() {
		this.about_store = new Dragonstone.Store.About();
		this.bookmarks = new Dragonstone.Registry.BookmarkRegistry();
		this.cache = new Dragonstone.Store.Cache();
		this.core_settings_provider = new Dragonstone.Settings.Context.Fallback();
		this.bookmarks_settings_bride = new Dragonstone.Settings.Bridge.Bookmarks(core_settings_provider, "settings.bookmarks", bookmarks);
		this.frontend_settings = new Dragonstone.Settings.Bridge.KV(core_settings_provider, "settings.frontend.kv");
		this.gopher_type_registry = new Dragonstone.Registry.GopherTypeRegistry.default_configuration();
		this.gtk_view_registry = new Dragonstone.GtkUi.LegacyViewRegistry.default_configuration(translation);
		this.mimeguesser = new Dragonstone.Registry.MimetypeGuesser.default_configuration();
		this.session_registry = new Dragonstone.Registry.SessionRegistry();
		this.translation = new Dragonstone.Registry.TranslationMultiplexerRegistry();
		this.uri_autoprefixer = new Dragonstone.Registry.UriAutoprefix();
		this.stores = new Dragonstone.Registry.StoreRegistry();
		string cachedir = GLib.Environment.get_user_cache_dir();
		this.main_store = new Dragonstone.Store.Switch(cachedir+"/dragonstone", stores, cache);
		this.stores.add_resource_store("about:", about_store);
	}
	/*
	private HashTable<string,Object> objects = new HashTable<string,Object>(str_hash, str_equal);
	
	public void store(string key,Object val){
		objects.set(key,val);
	}
	
	public Object retrieve(string key){
		return objects.get(key);
	}
	
	public void @foreach(HFunc<string,Object> cb){
		objects.@foreach(cb);
	}
	*/
	
}

/*
errordomain Dragonstone.SuperRegistryError {
    MISSING_ENTRY
}*/
